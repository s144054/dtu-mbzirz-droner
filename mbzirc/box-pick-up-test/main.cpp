/*! @file flight-control/main.cpp
 *  @version 3.3
 *  @date Jun 05 2017
 *
 *  @brief
 *  main for Flight Control API usage in a Linux environment.
 *  Provides a number of helpful additions to core API calls,
 *  especially for position control, attitude control, takeoff,
 *  landing.
 *
 *  @Copyright (c) 2016-2017 DJI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */ 
 //K-develope

// DJI OSDK includes
#include "dji_status.hpp"
#include <dji_vehicle.hpp>
// Helpers
#include <dji_linux_helpers.hpp>

#include "CameraLocalizer.hpp"
#include "Rangefinder.hpp"
#include "LaserScanner.hpp"

#include "PIDPositionControl.hpp"

#include <cmath>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;


/*! main
 *
 */
int
main(int argc, char** argv)
{

    // Initialize variables
    int functionTimeout = 1;

    // Setup OSDK.
    LinuxSetup linuxEnvironment(argc, argv);
    Vehicle*   vehicle = linuxEnvironment.getVehicle();
    if (vehicle == NULL)
    {
        std::cout << "Vehicle not initialized, exiting.\n";
        return -1;
    }

    // Obtain Control Authority
    vehicle->obtainCtrlAuthority(functionTimeout);
    
    // Start sensors  
    

    // Start Camera
    CameraLocalizer downCamera(640,480);//(640,480);
    if( !downCamera.init_camera(vehicle) ) {
        std::cout << "Camera couldn't start\n";
        return 0;
    }
    // Sleep to ensure camera functioning properly
    sleep(1);
    downCamera.set_loopFreq(30);
    

    Rangefinder rangefinder;
    if( !rangefinder.initializeI2C() ) {
        std::cout << "Rangefinder not connected\n";
    }
    
    // Initialize the controller and Kalman filter model
    PIDPositionControl controller;
    controller.init_controller(vehicle);
    
    // Add sensors to the controller and reset position to camera
    controller.set_camera_localizer( &downCamera );
    controller.set_rangefinder( &rangefinder );
    //controller.reset_position_camera();
    controller.set_reference(0, 0, 0, 0);
    
    pose_t camera_pose = downCamera.get_camera_position();
    pose_t position = controller.get_position();
    pose_t gps_pos = controller.get_GPS_position();
    target_pos reference = controller.get_reference();
    
    std::cout << "#######################################\n" << std::endl;
    std::cout << "Rangefinder height = " << rangefinder.getHeight() << " m\n\n";
    std::cout << "Camera position:\nX = " << camera_pose.x << " m\nY = " << camera_pose.y << " m\nTh = " << camera_pose.th*180/M_PI << " deg\nValid = " << camera_pose.valid << "Num pose = " << camera_pose.num_pose << "\n\n";
    std::cout << "GPS position:\nX = " << gps_pos.x << " m\nY = " << gps_pos.y << " m\nTh = " << gps_pos.th*180/M_PI << " deg\nValid = " << gps_pos.valid << "\n\n";
    std::cout << "Kalman position:\nX = " << position.x << " m\nY = " << position.y << " m\nTh = " << position.th*180/M_PI << " deg\n\n";
    std::cout << "Reference position:\nX = " << reference.x << " m\nY = " << reference.y << " m\nTh = " << reference.th*180/M_PI << " deg\n\n";
    std::cout << "######################################\n";
    
    // Display interactive prompt
	std::cout << "| Available commands:        |" << std::endl;
	std::cout << "| Press [a]: Start Stay At Home with GPS start and landing   |" << std::endl;
    std::cout << "| Press [b]: Start Stay At Home with Camera start and landing   |" << std::endl;
    
    char inputChar;
	std::cin >> inputChar;
    
    controller.set_simulation(0);
    
    switch(inputChar){
        case 'a':
            
            std::cout << "Full run test\n";
            downCamera.global_map.decode_xml("maps/2_boxes.xml");
            controller.set_global_estimator(GPS);
            std::cin >> inputChar;
            
            
            
            std::cout << "Takeoff\n";
            controller.takeoff(1.5);
            controller.waitForPositionReached(15);
            std::cout << "Takeoff complete moving to first box (3,-3)\n";
            controller.set_reference(0, -2, 1.5 ,0);
            controller.waitForPositionReached(15);
//             controller.set_desired_height(1.0);
//             controller.waitForPositionReached(15);
            std::cout << "Move to box complete\n";
            
            
            std::cin >> inputChar;
            std::cout << "Switching to CAMERA\n";
            controller.switch_to_camera_localizer();
            controller.waitForPositionReached(15);
            controller.set_reference(0, -0.1, 1.0 ,M_PI/2);
            controller.waitForPositionReached(15);
            controller.set_desired_height(0.5);
            controller.waitForPositionReached(15);
            std::cout << "Pickup height reached\n";
            
            std::cin >> inputChar;
            std::cout << "Switching back to GPS\n";
            rangefinder.set_in_use( false );
            controller.switch_to_GPS_localizer();
            controller.set_desired_height(2.3);
            controller.waitForPositionReached(15);
            
            std::cout << "Ready to turn on rangefinder\n";
            std::cin >> inputChar;
            rangefinder.set_in_use( true );
            
            std::cin >> inputChar;
            std::cout << "Ready to return home\n";
            controller.set_reference(0, 0, 1.7 ,0);
            controller.waitForPositionReached(15);
            
            
//             std::cin >> inputChar;
            std::cout << "Landing\n";
            controller.land_copter();
            std::cout << "Task complete\n";
            
            break;
            
        default:
        break;
    }
    
    downCamera.stop_camera();
    controller.stop_controller();
    
    vehicle->releaseCtrlAuthority(functionTimeout);
    
    return 0;
}
