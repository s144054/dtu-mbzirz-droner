/*! @file flight-control/main.cpp
 *  @version 3.3
 *  @date Jun 05 2017
 *
 *  @brief
 *  main for Flight Control API usage in a Linux environment.
 *  Provides a number of helpful additions to core API calls,
 *  especially for position control, attitude control, takeoff,
 *  landing.
 *
 *  @Copyright (c) 2016-2017 DJI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */ 
 //K-develope

#include "CameraLocalizer.hpp"
#include "Rangefinder.hpp"
#include "LaserScanner.hpp"

#include <iostream>

// DJI OSDK includes
#include "dji_status.hpp"
#include <dji_vehicle.hpp>
// Helpers
#include <dji_linux_helpers.hpp>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;




/*! main
 *
 */
int
main(int argc, char** argv)
{

    // Display interactive prompt
    std::cout << "| Available commands:              |" << std::endl;
    std::cout << "| Press [a]: Test Rangefinder      |" << std::endl;
    std::cout << "| Press [b]: Test laser scanner    |" << std::endl;
    std::cout << "| Press [c]: Test camera           |" << std::endl;
    char inputChar;
    std::cin >> inputChar;

    // Initialize variables
    int functionTimeout = 1;

    // Setup OSDK.
    LinuxSetup linuxEnvironment(argc, argv);
    Vehicle*   vehicle = linuxEnvironment.getVehicle();
    if (vehicle == NULL)
    {
        std::cout << "Vehicle not initialized, exiting.\n";
        return -1;
    }

    Rangefinder rangefinder;
    CameraLocalizer downCamera(320,240);//(640,480);
    LaserScanner laserscanner;
    
    
  switch(inputChar){
    case 'a':
        if( !rangefinder.initializeI2C() ) break;
      
      for( int i = 0; i<10 ; i ++ ){
          std::cout << (i+1) << "/10: Rf = " << rangefinder.getHeight() << std::endl;
          sleep(1);
      }

      break;
      
    case 'b':
        if( !laserscanner.init_laser() ) break;
        laserscanner.test_zoneobst();
        break;
    
    case 'c':
        if( !downCamera.init_camera(vehicle) ) break;
        sleep(1);
        //downCamera.set_show_image(1);
        downCamera.set_loopFreq(15);
        sleep(10);
        downCamera.stop_camera();
        break;
    
    default:
      break;
  }
/*
  if( !shutdown(lmssrv.sockfd,SHUT_RDWR) )
    std::cout << "Socket closed\n";
*/
  return 0;
}
