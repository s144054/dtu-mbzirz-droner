/*! @file flight-control/main.cpp
 *  @version 3.3
 *  @date Jun 05 2017
 *
 *  @brief
 *  main for Flight Control API usage in a Linux environment.
 *  Provides a number of helpful additions to core API calls,
 *  especially for position control, attitude control, takeoff,
 *  landing.
 *
 *  @Copyright (c) 2016-2017 DJI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */ 
 //K-develope

// DJI OSDK includes
#include "dji_status.hpp"
#include <dji_vehicle.hpp>
// Helpers
#include <dji_linux_helpers.hpp>

#include "CameraLocalizer.hpp"
#include "Rangefinder.hpp"
#include "LaserScanner.hpp"
#include "autnav_control.hpp"

#include "flight_control_sample.hpp"

#include <cmath>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;


/*! main
 *
 */
int
main(int argc, char** argv)
{

  // Initialize variables
  int functionTimeout = 1;

  // Setup OSDK.
  LinuxSetup linuxEnvironment(argc, argv);
  Vehicle*   vehicle = linuxEnvironment.getVehicle();
  if (vehicle == NULL)
  {
    std::cout << "Vehicle not initialized, exiting.\n";
    return -1;
  }

  // Obtain Control Authority
  vehicle->obtainCtrlAuthority(functionTimeout);
  
  // Start sensors  
  
  // Start fake Camera
  CameraLocalizer downCamera(320,240);//(640,480);
  downCamera.reset_position();
  
  Rangefinder rangefinder;
  if( !rangefinder.initializeI2C() ) {
    std::cout << "Rangefinder not connected\n";
  }
  
  // Initialize the controller and Kalman filter model
  ControlAutnav controller;
  controller.autnav_init(vehicle);
  
  // Add sensors to the controller and reset position to camera
  controller.set_camera_localizer( &downCamera );
  controller.set_rangefinder( &rangefinder );
  controller.reset_position_camera();
  
  pose_t camera_pose = downCamera.get_camera_position();
  pose_t position = controller.get_position();
  target_pos reference = controller.get_reference();
  
  std::cout << "#######################################\n" << std::endl;
  std::cout << "Rangefinder height = " << rangefinder.getHeight() << " m\n\n";
  std::cout << "Camera position:\nX = " << camera_pose.x << " m\nY = " << camera_pose.y << " m\nTh = " << camera_pose.th*180/M_PI << " deg\nValid = " << camera_pose.valid << "Num pose = " << camera_pose.num_pose << "\n\n";
  std::cout << "Kalman position:\nX = " << position.x << " m\nY = " << position.y << " m\nTh = " << position.th*180/M_PI << " deg\n\n";
  std::cout << "Reference position:\nX = " << reference.x << " m\nY = " << reference.y << " m\nTh = " << reference.th*180/M_PI << " deg\n\n";
  std::cout << "######################################\n";
  
  // Display interactive prompt
	std::cout << "| Available commands:        |" << std::endl;
	std::cout << "| Press [a]: Start flight mission (no camera) with GPS start and landing   |" << std::endl;
    std::cout << "| Press [b]: Run just localizer without controller outputs" << std::endl;
	
    pose_t kalman_pos;
    float zvelcmd = 0;
    
    char inputChar;
	std::cin >> inputChar;

    
  switch(inputChar){
    case 'a':
//         downCamera.stop_camera();
        monitoredTakeoff(vehicle);

        std::cout << "########## Starting Controller ###########\n";
        
        //controller.set_desired_height(1.7);
        controller.set_desired_height( rangefinder.getHeight() );
        controller.enable_controller(true);
        
        for(int i = 0; i < 20*5; i++){
            usleep(200000);
            if( i % 5 == 0 ) std::cout << "Time = " << i*0.2 << " seconds\n";
                if(i == 1*5) {
                    controller.set_reference(1, 1, rangefinder.getHeight(), -M_PI/4);
                }
                if(i == 15*5) {
                    controller.set_reference(-2, -1, rangefinder.getHeight(), M_PI/7);
                }
                if(i == 30*5) {
                    controller.set_reference(0, 0, rangefinder.getHeight(), 0);
                }
        }
        
        std::cout << "########## Stopping Controller ###########\n";
        controller.disable_controller();
        
        monitoredLanding(vehicle);
        
            
        break;
    case 'b':

        break;
    default:
      break;
  }
  
  controller.stop_autnav();
  
  vehicle->releaseCtrlAuthority(functionTimeout);
  
  return 0;
}
