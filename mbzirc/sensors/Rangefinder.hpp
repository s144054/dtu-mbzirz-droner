#ifndef RANGEFINDER_HPP
#define RANGEFINDER_HPP

#include <unistd.h>				//Needed for I2C port
#include <fcntl.h>				//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <linux/i2c-dev.h>		//Needed for I2C port

class Rangefinder {

private:
    bool enabled;
    bool in_use = true;
    int file_i2c;
    
public:
    bool is_enabled();
    
    void set_in_use(bool val);
    
    /*! Initializing the Terraranger which is comunicating using I2C protocal
    !*/
    bool initializeI2C();

    /*! If connected to the Terraranger then getHeight will return the Terraranger height reading
        If not then it will return the height measurement from the integrated Matrice sensors
    !*/
    float getHeight();
    float getHeightRP( float roll, float pitch );

};

#endif
