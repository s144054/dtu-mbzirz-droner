#ifndef _CAMERA_LOCALIZER_PI_HPP
#define _CAMERA_LOCALIZER_PI_HPP

#include <raspicam/raspicam_cv.h> 
#include "aruco_map.h"
//#include "aruco_pose.h"

#include "type_defines.hpp"

#include "loop-heartbeat.hpp"
#include <pthread.h>

#include "dji_vehicle.hpp"

class CameraLocalizer : public InternalThread{
public:
    //CameraLocalizer() {}
    CameraLocalizer(int w = 320, int h = 240) {res_width = w; res_height = h;}
    ~CameraLocalizer() {}
    
	bool init_camera( Vehicle * v );
    bool stop_camera();
    
    bool is_valid();
    
    void set_show_image(bool enable);
    
    void reset_valid();
    
    void reset_position();
    
    void set_loopFreq(int freq);
    
    pose_t get_camera_position();
    
    ar_map global_map;

private:
	
    void cameraMainLoop();
    void InternalThreadEntry();
    
    
    HeartbeatTimer loopTimer;
    
	int res_width = 320;
	int res_height = 240;
    int default_freq = 10;
    
    int loopFreq = 10;
    
    int show_image_freq = 2;
    
    bool show_image = false;
    
    bool camera_running = false;
    
    pose_t camera_position_estimate;
    
    pthread_mutex_t pose_est_mutex = PTHREAD_MUTEX_INITIALIZER;
    
    DJI::OSDK::Vehicle * vehicle;
    
	raspicam::RaspiCam_Cv piCamera;

};

#endif
