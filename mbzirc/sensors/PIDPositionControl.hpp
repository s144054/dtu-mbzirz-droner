#ifndef PIDPOSITIONCONTROL_HPP
#define PIDPOSITIONCONTROL_HPP

#include "ControllerBase.hpp"
#include "type_defines.hpp"
#include "pid.h"

class PIDPositionControl : public ControllerBase {
    
private:
    
    void controller_run();
    void controllerMainLoop();
    
    float F[9][9] = {{0.0}};
    float G[9][4] = {{0.0}};
    float L[9][6] = {{0.0}};
    
    float inno[6] = {0.0};

    float yawratecmd= 0;
    float xcmd = 0;
    float ycmd  = 0;
    float zvelcmd = 0;

    int num_states = 9;
    int num_meas   = 6;
    
    PID height_pid = PID(Ts, 3, -3, 0.9, 0.0, 0.0);
    PID yaw_pid = PID(Ts, 40, -40, 0.9, 0.0, 0.0);
    PID x_pid = PID(Ts, 1.7, -1.7, 1.2, 0.5, 0.1);
    PID y_pid = PID(Ts, 1.7, -1.7, 1.2, 0.5, 0.1);
    
public:
    
    bool init_controller(Vehicle * v);
    
    void switch_to_camera_localizer();
    void switch_to_GPS_localizer();
    
};

#endif
