#include "autnav_control.hpp"
#include "loop-heartbeat.hpp"
#include "time.h"
#include "GeneralCommands.hpp"
#include <vector>
#include <cmath>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;

void ControlAutnav::InternalThreadEntry()
{
    autnav_running = true;
    std::cout << "InternalThreadEntry\n";
    autnavMainLoop();
}

bool ControlAutnav::stop_autnav(){
    autnav_running = false;
    WaitForInternalThreadToExit();
    loopTimer.stop_hb();
}


void ControlAutnav::set_camera_localizer(CameraLocalizer* cam)
{
    cameraLocalizer = cam;
}

void ControlAutnav::set_rangefinder(Rangefinder* rf)
{
    rangefinder = rf;
    
}


void ControlAutnav::enable_controller(bool integral){
    controller_enable = 1;
    if( integral ){
        ix = 0;
        iy = 0;
    }
    integral_enable = integral;
}

void ControlAutnav::disable_controller(){
    controller_enable = 0;
    vehicle->control->attitudeYawRateAndVertVelCtrl(0, 0, 0, 0);
}


// newmode _init - initialise althold controller and variables
bool ControlAutnav::autnav_init(Vehicle * v)
{
    vehicle = v;
    
    uint8_t broadcast_freq[16];
    vehicle->broadcast->setVersionDefaults(broadcast_freq);
    broadcast_freq[1] = DataBroadcast::FREQ_100HZ; // attitude
    broadcast_freq[2] = DataBroadcast::FREQ_100HZ; // acceleration
    broadcast_freq[3] = DataBroadcast::FREQ_0HZ; // linear velocity
    broadcast_freq[4] = DataBroadcast::FREQ_100HZ; // gyro
    broadcast_freq[5] = DataBroadcast::FREQ_50HZ; // gps location
    broadcast_freq[6] = DataBroadcast::FREQ_0HZ; // magnetometer
    //broadcast_freq[7] = DataBroadcast::FREQ_5HZ; // remote controller data
    broadcast_freq[8] = DataBroadcast::FREQ_0HZ; // Gimbal
    //broadcast_freq[9] = DataBroadcast::FREQ_50HZ; // flightstatus
    //broadcast_freq[10] = DataBroadcast::FREQ_50HZ; //battery
    //broadcast_freq[11] = DataBroadcast::FREQ_50HZ; //control device
    vehicle->broadcast->setBroadcastFreq(broadcast_freq);
    
    // Reset state vector
    for(int i = 0; i<num_states; i++){
        state[i] = 0.0f;
        last_state[i] = 0.0f;
    }

    // Reset innovation
    for(int i = 0; i<num_meas; i++){
        inno[i] = 0.0f;
    };

    // Reset target pose
    pose_target.x = 0.0f;
    pose_target.y = 0.0f;
    pose_target.z = 0.0f;
    pose_target.th = 0.0f;

    // Reset integral action
    ix = 0.0f;
    iy = 0.0f;
    
    integral_enable = 0;
    
    // System matrix
    F[0][0]=0.8878; F[0][1]=0.0f;    F[0][2]=0.0f; 
    F[1][0]=0.0314; F[1][1]=1.0f;    F[1][2]=0.0f; 
    F[2][0]=0.0005; F[2][1]=0.0333;  F[2][2]=1.0f; 

    F[3][3]=0.8878; F[3][4]=0.0f;    F[3][5]=0.0f; 
    F[4][3]=0.0314; F[4][4]=1.0f;    F[4][5]=0.0f; 
    F[5][3]=0.0005; F[5][4]=0.0333;  F[5][5]=1.0f; 

    F[6][6]=1.0;    F[6][7]=0.0f;    F[6][8]=0.0f; 
    F[7][6]=0.0333; F[7][7]=1.0f;    F[7][8]=0.0f; 
    F[8][6]=0.0;    F[8][7]=0.0;     F[8][8]=1.0f; 

    // Input matrix
    G[0][0]= 1.1021;
    G[1][0]= 0.0187; 
    G[2][0]= 0.0002; 

    G[3][1]= 1.1021;
    G[4][1]= 0.0187; 
    G[5][1]= 0.0002; 
    
    G[6][2] = 0.0333;
    G[7][2] = 0.0006;
    G[8][3] = 0.0333;

    for( int i = 0; i < 9; i++ ){
        for( int j = 0; j < 6; j++ ){
            L[i][j] = 0.0f;
        }
    }
    
    L[0][0] = 0.9403;
    L[0][1] = 0.0238;
    L[1][0] = 0.0016;
    L[1][1] = 0.8728;
    L[2][1] = 0.2267;
    
    L[3][2] = 0.9403;
    L[3][3] = 0.0238;
    L[4][2] = 0.0016;
    L[4][3] = 0.8728;
    L[5][3] = 0.2267;
    
    L[6][4] = 0.2941;
    L[7][4] = 0.1352;
    
    L[8][5] = 0.6180;

    K[0] = 0.1135;
    K[1] = 0.6350;
    K[2] = 1.0755;
    
    Ki =  0.0295;

    global_vel_ref.x = 2;
    global_vel_ref.y = 2;
    global_vel_ref.z = 1;
    global_vel_ref.th = 40*M_PI/180;
    
    originGPS_Position = vehicle->broadcast->getGlobalPosition();
    
    loopTimer.start_hb(loopFreq);
    
    if( !StartInternalThread() ){
        std::cout << "Autnav control thread not started\n";
        return false;
    }
    
    return true;
}

// autnav_run - runs the autonomous navigation routine, which gets a global position from a companion computer send on
// serial4/5. The control loop runs at 400 Hz
//void ControlAutnav::autnav_run()
void ControlAutnav::autnav_run(float x, float y, float z, float th)
{
    
    if( true ){
        
        Telemetry::Quaternion quaterion = vehicle->broadcast->getQuaternion();
        Telemetry::Vector3f attitude = toEulerAngle(&quaterion);
        Telemetry::Vector3f acc = vehicle->broadcast->getAcceleration();
        Telemetry::Vector3f gyro = vehicle->broadcast->getAngularRate();

//         Telemetry::Vector3f attitude;
//         Telemetry::Vector3f acc;
//         Telemetry::Vector3f gyro;

//         attitude.x = 0;
//         attitude.y = 0;
//         acc.x = 0;
//         acc.y = 0;
//         acc.z = 0;
//         gyro.z = 0;
        
        for(int i = 0; i<num_states; i++){
            state[i] = 0.0f;
        }
        
        // Kalman prediction
        for(int i = 0; i<num_states; i++){
            state.xacc += F[0][i]*last_state[i];
            state.xvel += F[1][i]*last_state[i];
            state.xpos += F[2][i]*last_state[i];
            state.yacc += F[3][i]*last_state[i];
            state.yvel += F[4][i]*last_state[i];
            state.ypos += F[5][i]*last_state[i];
            state.zvel += F[6][i]*last_state[i];
            state.zpos += F[7][i]*last_state[i];
            state.head += F[8][i]*last_state[i];
        }

        // Inject input to model

        state.xacc += G[0][0]*( pitchcmd*M_PI/180.0 );
        state.xvel += G[1][0]*( pitchcmd*M_PI/180.0 );
        state.xpos += G[2][0]*( pitchcmd*M_PI/180.0 );
        state.yacc += G[3][1]*( rollcmd*M_PI/180.0 );
        state.yvel += G[4][1]*( rollcmd*M_PI/180.0 );
        state.ypos += G[5][1]*( rollcmd*M_PI/180.0 );
        
        state.zvel += G[6][2]*(acc.z); 
        state.zpos += G[7][2]*(acc.z);           
        state.head += G[8][3]*(-gyro.z);   

        
        acc = rotation_pixhawk_to_world(acc.x, acc.y, acc.z, last_state.head);
        // Innovation using acceleration measurement
        inno[0] = acc.x - last_state.xacc;
        inno[2] = acc.y - last_state.yacc;
        
        if( z != 0 ){
            inno[4] = (z - last_state.zpos);
        }
        else{
            inno[4] = 0;
        }
    
        if( x == 0 and y == 0) {
            inno[1] = 0;
            inno[3] = 0;
            inno[5] = 0;
        }
        else{
            inno[1] = x - last_state.xpos;
            inno[3] = y - last_state.ypos; 
            inno[5] = th - last_state.head;
        }
        
//         float rf_h = rangefinder->getHeightRP(attitude.x, attitude.y);
//         
//         if( rf_h != 0 )
//             inno[4] = (rf_h - last_state.zpos);
//         else
//             inno[4] = 0;
//         
//         // Innovation using global pose - only when new localization data is ready from odroid,
//         // otherwise set to zero
//         
//         pose_t cam_pos = cameraLocalizer->get_camera_position();
//         if ( cam_pos.valid && (cam_pos.num_pose > old_num_pose) ) { // new pose from localizer
//             
//                 //std::cout << "campose updated\n";
//                 inno[1] = cam_pos.x - last_state.xpos;
//                 inno[3] = cam_pos.y - last_state.ypos; 
//                 inno[5] = cam_pos.th - last_state.head;
//                 //cameraLocalizer->reset_valid();
//                 old_num_pose = cam_pos.num_pose;
//         }
//         else{
//             inno[1] = 0.0f;
//             inno[3] = 0.0f; 
//             inno[5] = 0.0f;
//         }
        
        // Kalman update
        for(int i = 0; i<num_meas; i++){
            state.xacc += L[0][i]*inno[i];
            state.xvel += L[1][i]*inno[i];
            state.xpos += L[2][i]*inno[i];
            state.yacc += L[3][i]*inno[i];
            state.yvel += L[4][i]*inno[i];
            state.ypos += L[5][i]*inno[i];
            state.zvel += L[6][i]*inno[i];
            state.zpos += L[7][i]*inno[i];
            state.head += L[8][i]*inno[i];
        }
        
        // Update last state
        last_state.xacc = state.xacc;
        last_state.xvel = state.xvel;
        last_state.xpos = state.xpos;
        last_state.yacc = state.yacc;
        last_state.yvel = state.yvel;
        last_state.ypos = state.ypos;
        last_state.zvel = state.zvel;
        last_state.zpos = state.zpos;
        last_state.head = state.head;
        
        if( controller_enable ) {
            
            // Reference generator
            if(fabs(pose_target.x - global_pose_ref.x)<=0.01f){
                pose_target.x = global_pose_ref.x;
            }
            else{
                if(pose_target.x < global_pose_ref.x){
                    pose_target.x = pose_target.x + global_vel_ref.x*Ts;
                }
                else{
                    pose_target.x = pose_target.x - global_vel_ref.x*Ts;
                }
            }
            if(fabs(pose_target.y - global_pose_ref.y)<=0.01f){
                pose_target.y = global_pose_ref.y;
            }
            else{
                if(pose_target.y < global_pose_ref.y){
                    pose_target.y = pose_target.y + global_vel_ref.y*Ts;
                }
                else{
                    pose_target.y = pose_target.y - global_vel_ref.y*Ts;
                }
            }
            if(fabs(pose_target.z - global_pose_ref.z)<=0.001f){
                pose_target.z = global_pose_ref.z;
            }
            else{
                if(pose_target.z < global_pose_ref.z){
                    pose_target.z = pose_target.z + global_vel_ref.z*Ts;
                }
                else{
                    pose_target.z = pose_target.z - global_vel_ref.z*Ts;
                }
            }
            if(fabs(pose_target.th - global_pose_ref.th)<=0.001f){
                pose_target.th = global_pose_ref.th;
            }
            else{
                if(pose_target.th < global_pose_ref.th){
                    pose_target.th = pose_target.th + global_vel_ref.th*Ts;
                }
                else{
                    pose_target.th = pose_target.th - global_vel_ref.th*Ts;
                }
            }
        

            
            // Control action with anti-wind-up
            if(integral_enable){
                pitchcmd    =   ix*Ki - (K[0]*state.xacc + K[1]*state.xvel + K[2]*state.xpos);
                
                rollcmd     =   iy*Ki - (K[0]*state.yacc + K[1]*state.yvel + K[2]*state.ypos);

                if(1){ //if flying
                    ix = ix + (pose_target.x  - state.xpos);
                    iy = iy + (pose_target.y  - state.ypos);
                }
            }
            else{
                pitchcmd    =   pose_target.x - (K[0]*state.xacc + K[1]*state.xvel + K[2]*state.xpos);
                rollcmd     =   pose_target.y - (K[0]*state.yacc + K[1]*state.yvel + K[2]*state.ypos);
            }
            
            yawratecmd = yaw_pid.calculate(pose_target.th, state.head);
            
            zvelcmd = height_pid.calculate(global_pose_ref.z, state.zpos);
            
            // Add control action to attitude controller variables and convert from rad to centidegrees: 180/pi*100 = 5730
            float target_roll, target_pitch, target_yaw_rate;
            target_pitch    = -pitchcmd*180/M_PI;
            target_roll     = -rollcmd*180/M_PI;
            target_yaw_rate = -yawratecmd*180/M_PI;

            // Constrain control signal in centidegrees
            float pitchmax = 20;
            float rollmax = 20;
            float yawratemax = 45;
            
            if(target_pitch > pitchmax){
                target_pitch =  pitchmax;   
                pitchcmd_after_limit =  pitchmax;
            }
            else if(target_pitch < -pitchmax){
                target_pitch = -pitchmax;  
                pitchcmd_after_limit = -pitchmax;
            }
            else{ 
                pitchcmd_after_limit = pitchcmd;
            }

            if(target_roll  >  rollmax){
                target_roll  =  rollmax;    
                rollcmd_after_limit  =  rollmax;
            }
            else if(target_roll  < -rollmax){
                target_roll   = -rollmax;    
                rollcmd_after_limit  = -rollmax;
            }
            else{
                rollcmd_after_limit = rollcmd;
            }

            if(target_yaw_rate >  yawratemax){target_yaw_rate =  yawratemax;}
            if(target_yaw_rate < -yawratemax){target_yaw_rate = -yawratemax;}   
            
            Vector3f control_signal = rotation_world_to_pixhawk(target_pitch, target_roll, 0, state.head);
            
            pitchcmd   = target_pitch;
            rollcmd    = target_roll;
            yawratecmd = target_yaw_rate;
            
            rot_rollcmd = control_signal.y;
            rot_pitchcmd = control_signal.x;
            
            vehicle->control->attitudeYawRateAndVertVelCtrl( control_signal.y, control_signal.x, yawratecmd, zvelcmd);

        }
    }
}

//void ControlAutnav::position_control_run(float x, float y, float z, float th)
void ControlAutnav::position_control_run(bool simulation)
{
    
    pose_t globalPos;
    
    if( globalEstimatorUsed == CAMERA ) globalPos = cameraLocalizer->get_camera_position();
    else if( globalEstimatorUsed == GPS ) globalPos = get_GPS_position();
    
    Telemetry::Quaternion quaterion = vehicle->broadcast->getQuaternion();
    Telemetry::Vector3f attitude = toEulerAngle(&quaterion);
    Telemetry::Vector3f acc = vehicle->broadcast->getAcceleration();
    Telemetry::Vector3f gyro = vehicle->broadcast->getAngularRate();

//         Telemetry::Vector3f attitude;
//         Telemetry::Vector3f acc;
//         Telemetry::Vector3f gyro;

//         attitude.x = 0;
//         attitude.y = 0;
//         acc.x = 0;
//         acc.y = 0;
//         acc.z = 0;
//         gyro.z = 0;
    
    for(int i = 0; i<num_states; i++){
        state[i] = 0.0f;
    }
    
    // Kalman prediction
    for(int i = 0; i<num_states; i++){
        state.xacc += F[0][i]*last_state[i];
        state.xvel += F[1][i]*last_state[i];
        state.xpos += F[2][i]*last_state[i];
        state.yacc += F[3][i]*last_state[i];
        state.yvel += F[4][i]*last_state[i];
        state.ypos += F[5][i]*last_state[i];
        state.zvel += F[6][i]*last_state[i];
        state.zpos += F[7][i]*last_state[i];
        state.head += F[8][i]*last_state[i];
    }

    // Inject input to model

    state.xacc += G[0][0]*( pitchcmd*M_PI/180.0 );
    state.xvel += G[1][0]*( pitchcmd*M_PI/180.0 );
    state.xpos += G[2][0]*( pitchcmd*M_PI/180.0 );
    state.yacc += G[3][1]*( rollcmd*M_PI/180.0 );
    state.yvel += G[4][1]*( rollcmd*M_PI/180.0 );
    state.ypos += G[5][1]*( rollcmd*M_PI/180.0 );
    
    state.zvel += G[6][2]*(acc.z); 
    state.zpos += G[7][2]*(acc.z);           
    state.head += G[8][3]*(-gyro.z);   
    
    acc = rotation_pixhawk_to_world(acc.x, acc.y, acc.z, last_state.head);
    // Innovation using acceleration measurement
    inno[0] = acc.x - last_state.xacc;
    inno[2] = acc.y - last_state.yacc;
    
    if( !simulation ) globalPos.z = rangefinder->getHeightRP(attitude.x, attitude.y);
    
    if( globalPos.z != 0 ){
        inno[4] = (globalPos.z - last_state.zpos);
    }
    else{
        inno[4] = 0;
    }

    if( globalPos.valid ) {
        inno[1] = globalPos.x - last_state.xpos;
        inno[3] = globalPos.y - last_state.ypos; 
        inno[5] = globalPos.th - last_state.head;
    }
    else {
        inno[1] = 0;
        inno[3] = 0; 
        inno[5] = 0;
    }
    
//         float rf_h = rangefinder->getHeightRP(attitude.x, attitude.y);
//         
//         if( rf_h != 0 )
//             inno[4] = (rf_h - last_state.zpos);
//         else
//             inno[4] = 0;
//         
//         // Innovation using global pose - only when new localization data is ready from odroid,
//         // otherwise set to zero
//         
//         pose_t cam_pos = cameraLocalizer->get_camera_position();
//         if ( cam_pos.valid && (cam_pos.num_pose > old_num_pose) ) { // new pose from localizer
//             
//                 //std::cout << "campose updated\n";
//                 inno[1] = cam_pos.x - last_state.xpos;
//                 inno[3] = cam_pos.y - last_state.ypos; 
//                 inno[5] = cam_pos.th - last_state.head;
//                 //cameraLocalizer->reset_valid();
//                 old_num_pose = cam_pos.num_pose;
//         }
//         else{
//             inno[1] = 0.0f;
//             inno[3] = 0.0f; 
//             inno[5] = 0.0f;
//         }
    
    // Kalman update
    for(int i = 0; i<num_meas; i++){
        state.xacc += L[0][i]*inno[i];
        state.xvel += L[1][i]*inno[i];
        state.xpos += L[2][i]*inno[i];
        state.yacc += L[3][i]*inno[i];
        state.yvel += L[4][i]*inno[i];
        state.ypos += L[5][i]*inno[i];
        state.zvel += L[6][i]*inno[i];
        state.zpos += L[7][i]*inno[i];
        state.head += L[8][i]*inno[i];
    }
    
    // Update last state
    last_state.xacc = state.xacc;
    last_state.xvel = state.xvel;
    last_state.xpos = state.xpos;
    last_state.yacc = state.yacc;
    last_state.yvel = state.yvel;
    last_state.ypos = state.ypos;
    last_state.zvel = state.zvel;
    last_state.zpos = state.zpos;
    last_state.head = state.head;
    
    if( controller_enable ) {
    
        generate_Reference();
        
        yawratecmd = -yaw_pid.calculate(pose_target.th, state.head) * 180/M_PI;

        zvelcmd = height_pid.calculate(pose_target.z, state.zpos);

        xcmd = x_pid.calculate(pose_target.x, state.xpos);

        ycmd = y_pid.calculate(pose_target.y, state.ypos);

        Vector3f control_signal = rotation_world_to_pixhawk(xcmd, ycmd, 0, state.head);
        xcmd = control_signal.x;
        ycmd = -control_signal.y;
        vehicle->control->xyPosZvelAndYawRateCtrl(xcmd, ycmd, zvelcmd, yawratecmd);

//             vehicle->control->xyPosZvelAndYawRateCtrl(xcmd, ycmd, zvelcmd, yawratecmd);
    }

}

void ControlAutnav::generate_Reference( ) {

    float x_thresh = (global_vel_ref.x * Ts) * 1.1 ;
    float y_thresh = (global_vel_ref.y * Ts) * 1.1 ;
    float z_thresh = (global_vel_ref.z * Ts) * 1.1 ;
    float yaw_thresh = (global_vel_ref.th * Ts) * 1.1 ;
    
        // Reference generator
    if(fabs(pose_target.x - global_pose_ref.x) <= x_thresh){
        pose_target.x = global_pose_ref.x;
    }
    else{
        if(pose_target.x < global_pose_ref.x){
            pose_target.x = pose_target.x + global_vel_ref.x*Ts;
        }
        else{
            pose_target.x = pose_target.x - global_vel_ref.x*Ts;
        }
    }
    if(fabs(pose_target.y - global_pose_ref.y) <= y_thresh ){
        pose_target.y = global_pose_ref.y;
    }
    else{
        if(pose_target.y < global_pose_ref.y){
            pose_target.y = pose_target.y + global_vel_ref.y*Ts;
        }
        else{
            pose_target.y = pose_target.y - global_vel_ref.y*Ts;
        }
    }
    if(fabs(pose_target.z - global_pose_ref.z) <= z_thresh){
        pose_target.z = global_pose_ref.z;
    }
    else{
        if(pose_target.z < global_pose_ref.z){
            pose_target.z = pose_target.z + global_vel_ref.z*Ts;
        }
        else{
            pose_target.z = pose_target.z - global_vel_ref.z*Ts;
        }
    }
    if(fabs(pose_target.th - global_pose_ref.th) <= yaw_thresh){
        pose_target.th = global_pose_ref.th;
    }
    else{
        if(pose_target.th < global_pose_ref.th){
            pose_target.th = pose_target.th + global_vel_ref.th*Ts;
        }
        else{
            pose_target.th = pose_target.th - global_vel_ref.th*Ts;
        }
    }
    
}

void ControlAutnav::force_pose_target(){
    
    pose_target.x = global_pose_ref.x;
    pose_target.y = global_pose_ref.y;
    pose_target.z = global_pose_ref.z;
    pose_target.th = global_pose_ref.th;
    
}

bool ControlAutnav::takeoff(float target_height){
    
    float takeoff_height = last_state.zpos;
    
//     if( takeoff_height == 0 ) return 0;
    
    std::cout << "Arming motors\n";
    if( vehicle->control->armMotors(1).data ){
        std::cout << "Motors not armed\n";
    }
    sleep(5);
    
    std::cout << "Motors armed\n";
    
    for(int i = 0; i < 15; i ++){
        vehicle->control->xyPosZvelAndYawRateCtrl(0,0,1,0);
        usleep(100000);
    }
    
    float delta = 0;
    int count = 0;
    
    set_reference(last_state.xpos, last_state.ypos, target_height, last_state.head);
    force_pose_target();
    enable_controller(false);
    
    while(1){
        if( last_state.zpos > takeoff_height + 0.1 ){
            status = FLYING;
            enable_controller(true);
            break;
        }
        usleep(1000);
    }
    std::cout << "Copter in the air\n";

    do{
        delta = fabs( last_state.zpos - target_height );
        usleep(1000);

        if( delta < 0.1 ) count++;
    }
    while( count < 20 );
    
    std::cout << "Target height reached\n";
    
    return 1;
        
}

pose_t ControlAutnav::get_GPS_position(){
    
    Telemetry::Vector3f localOffset;
    
    Telemetry::GlobalPosition currentBroadcastGP = vehicle->broadcast->getGlobalPosition();
    
    localOffsetFromGpsOffset(vehicle, localOffset,
                             static_cast<void*>(&currentBroadcastGP),
                             static_cast<void*>(&originGPS_Position));
    
    Telemetry::Quaternion broadcastQ         = vehicle->broadcast->getQuaternion();
    
    float yawInRad           = toEulerAngle((static_cast<void*>(&broadcastQ))).z;
    
    pose_t pos;
    pos.x = localOffset.x;
    pos.y = -localOffset.y;
    pos.z = localOffset.z + 0.2;
    pos.th = -yawInRad;
    pos.valid = 1;
    
    return pos;
}

bool ControlAutnav::land_copter()
{
    set_reference( last_state.xpos, last_state.ypos, last_state.zpos, last_state.head );
    sleep(1);
    //set_reference( last_state.xpos, last_state.ypos, 0.0, last_state.head );
    set_desired_height(0.0);
    
    std::cout << "Desending\n";
    while(1){
        usleep(1000);
        if( last_state.zpos < 0.5 ){
            std::cout << "Disabling integral controller" << std::endl;
            integral_enable = 0;
            break;
        }
    }
    
    int count = 0;
    std::cout << "Landing" << std::endl;
    while( count < 50 ){
        if( last_state.zpos < 0.35 ) count++;
        usleep(1000);
    }
    
    status = LANDED;
    
    disable_controller();
    
    for(int i = 0; i < 15; i ++){
        vehicle->control->xyPosZvelAndYawRateCtrl(0,0,-0.5,0);
        usleep(100000);
    }
    std::cout << "Disarming motors\n";
    for(int i = 0; i < 10; i ++){
        vehicle->control->disArmMotors(1);
        usleep(10000);
    }
    
}


void ControlAutnav::set_reference(float x, float y, float z, float th){
    global_pose_ref.x = x;
    global_pose_ref.y = y;
    global_pose_ref.z = z;
    global_pose_ref.th = th;
    
}

void ControlAutnav::set_desired_height(float z){
    global_pose_ref.z = z;
}
    
void ControlAutnav::reset_position_camera(){
    pose_t camera_pose = cameraLocalizer->get_camera_position();
    
    last_state.xpos = camera_pose.x;
    last_state.xvel = 0;
    last_state.xacc = 0;
    last_state.ypos = camera_pose.y;
    last_state.xvel = 0;
    last_state.xacc = 0;
    last_state.head = camera_pose.th;
    last_state.zvel = 0;
    last_state.zpos = rangefinder->getHeight();
    
    pose_target.x = camera_pose.x;
    pose_target.y = camera_pose.y;
    pose_target.th = camera_pose.th;
    
    set_reference(camera_pose.x, camera_pose.y, last_state.zpos, camera_pose.th);
    
    ix = 0;
    iy = 0;
    
    
}

pose_t ControlAutnav::get_position(){
    pose_t position;
    position.x = last_state.xpos;
    position.y = last_state.ypos;
    position.z = last_state.zpos;
    position.th = last_state.head;
    return position;
}

target_pos ControlAutnav::get_reference(){
    target_pos reference;
    reference.x = global_pose_ref.x;
    reference.y = global_pose_ref.y;
    reference.z = global_pose_ref.z;
    reference.th = global_pose_ref.th;
    return reference;
}


void ControlAutnav::set_global_estimator(int globalEst){
    globalEstimatorUsed = globalEst;
}

void ControlAutnav::autnavMainLoop()
{
    long beats = 0;
    struct timespec gettime_now; 
    
    long int last_heartbeat = 0;
    long int new_heartbeat = 0;
    long int time_diff = 0;
    
    double last_time;
    double new_time;
    double log_time;
    double time_diff_f;
    double freq;
    
    Vector3f velocity;
    Vector3f attitude;
    Quaternion atti_quad;
    float height;
    Telemetry::Vector3f acc;
    pose_t camera_pose;
    
    clock_gettime(CLOCK_REALTIME, &gettime_now);
    last_heartbeat = gettime_now.tv_nsec;
    long start_log_time = gettime_now.tv_sec;
    
    last_time = (double)( gettime_now.tv_sec - start_log_time) + (double)gettime_now.tv_nsec/(1000000000); 
    
    char str[80];
	char buf[16]; // need a buffer for that
	int i=1;
	while (true){
        sprintf(buf,"%d",i);
		const char* p = buf;
		strcpy (str,"/home/local/DJI_Matrice100_Ananda/Log/output");
		strcat (str,p);
		strcat (str,".txt");
		std::ifstream fileExist(str);
		if(!fileExist)
        {
			break;
		}
		i++;
	}
    std::freopen( str, "w", stderr );
    std::cerr.precision(10);
    
    while( autnav_running ) {
        
        while( !loopTimer.get_hb_flag() ) usleep(20);
        
        //autnav_run();
//         Telemetry::GlobalPosition currentBroadcastGP = vehicle->broadcast->getGlobalPosition();
//         localOffsetFromGpsOffset(vehicle, localOffset,
//                              static_cast<void*>(&currentBroadcastGP),
//                              static_cast<void*>(&originGPS_Position));
//         Telemetry::Quaternion broadcastQ         = vehicle->broadcast->getQuaternion();
//         float yawInRad           = toEulerAngle((static_cast<void*>(&broadcastQ))).z;
//         attitude = toEulerAngle(&broadcastQ);
//         

//        camera_pose = cameraLocalizer->get_camera_position();
//         autnav_run(localOffset.x, -localOffset.y, height, -yawInRad);
//        autnav_run(camera_pose.x, camera_pose.y, height, camera_pose.th);

        position_control_run(false);
        
//         position_control_run(localOffset.x, -localOffset.y, localOffset.z, -yawInRad);
//         position_control_run(camera_pose.x, camera_pose.y, height, camera_pose.th);
       
        if( beats % 30 == 0 ){
            if(globalEstimatorUsed == GPS){
                pose_t gps_pos = get_GPS_position();
                std::cout << "GPS:   X = " << gps_pos.x << "\tY = " << gps_pos.y << "\tZ = " << gps_pos.z << "\tTh = " << gps_pos.th*180/M_PI << std::endl;
            }
            else if( globalEstimatorUsed == CAMERA ){
                camera_pose = cameraLocalizer->get_camera_position();
                std::cout << "Camer: X = " << camera_pose.x << "\tY = " << camera_pose.y << "\tZ = " << height << "\tTh = " << camera_pose.th*180/M_PI << std::endl;
            }
            
            std::cout << "State: X = " << last_state.xpos << "\tY = " << last_state.ypos << "\tZ = " << last_state.zpos << "\tTh = " << last_state.head *180/M_PI << std::endl;
        }
        
//         if( false ) {
//             //height = localOffset.z;
//             if( controller_enable ) PID_control_step( localOffset.x, -localOffset.y, height, -yawInRad );
//             if( beats % 15 == 0 ){
//                 std::cout << "X = " << localOffset.x << "\tY = " << -localOffset.y << "\tZ = " << height << "\tTh = " << -yawInRad*180/M_PI << std::endl;
//             }
//             
//         }
//         else{
//             camera_pose = cameraLocalizer->get_camera_position();        
//             
//             if( controller_enable ) PID_control_step( camera_pose.x, camera_pose.y, height, camera_pose.th );
//             if( beats % 15 == 0 ){
//                 std::cout << "X = " << camera_pose.x << "\tY = " << camera_pose.y << "\tZ = " << height << "\tTh = " << camera_pose.th*180/M_PI << std::endl;
//             }
//         }
        
        
        
        //if( controller_enable ) PID_control_step( camera_pose.x, camera_pose.y, height, camera_pose.th);
        
//         if(controller_enable ) PID_control_step( localOffset.x, -localOffset.y, localOffset.z, -yawInRad );
        //if(controller_enable) PID_control_step(localOffset.x, localOffset.y, localOffset.z, yawInRad);
        //if(controller_enable) PID_control_step(camera_pose.x, camera_pose.y, camera_pose.z, camera_pose.th);
//         std::cout << localOffset.x << "  " << localOffset.y << "  " << localOffset.z << "  " << yawInRad*180/M_PI << "\n";
//         std::cout << camera_pose.x << "  " << camera_pose.y << "  " << camera_pose.z << "  " << camera_pose.th*180/M_PI << "\n";
        
        clock_gettime(CLOCK_REALTIME, &gettime_now);
        
        log_time = (double)( gettime_now.tv_sec - start_log_time) + (double)gettime_now.tv_nsec/(1000000000.0); 
        new_time = (double)( gettime_now.tv_sec - start_log_time) + (double)gettime_now.tv_nsec/(1000000000.0); 
        

        freq = 1.0/(new_time - last_time);
        
        //std::cout << "new_time = " << new_time << "   last = " << last_time << "  dif = " << freq << std::endl;
        
        last_time = new_time;
        
        
//         if( beats % 100000 == 0 && 0) {
//             std::cout << "States:" << std::endl;
//             std::cout << "X: " << last_state.xpos << "  |  ";
//             std::cout << "Y: " << last_state.ypos << "  |  ";
//             std::cout << "Z: " << last_state.zpos << "  |  ";
//             std::cout << "h: " << last_state.head << std::endl << std::endl;
//         }
//         if( beats % 8 == 0) {
//             clock_gettime(CLOCK_REALTIME, &gettime_now);
//             new_heartbeat = gettime_now.tv_nsec;
//         
//             time_diff = new_heartbeat - last_heartbeat;
//         
//             std::cout << "Freq = " << 1.0/(time_diff / 1000000000.0) << std::endl;
// 
//         }
        
        
        
         if( beats % 1 == 0 ) {
            velocity = vehicle->broadcast->getVelocity();
            atti_quad = vehicle->broadcast->getQuaternion();
            attitude = toEulerAngle( &atti_quad );
            height = vehicle->broadcast->getGlobalPosition().altitude;
            camera_pose = cameraLocalizer->get_camera_position();
            
            acc = vehicle->broadcast->getAcceleration();
            
            //if(beats % 64 == 0) std::cout << attitude.x << "  " << attitude.y << std::endl;
            
            std::cerr << log_time << " " << freq
            << " " << attitude.x << " " << attitude.y << " " << attitude.z << " "
            << global_pose_ref.x << " " << global_pose_ref.y << " " << global_pose_ref.z << " " << global_pose_ref.th << " "
            << camera_pose.x << " " << camera_pose.y << " " << camera_pose.z << " " << camera_pose.th << " " << camera_pose.valid << " "
            << height << " " << rangefinder->getHeight() << " " << rangefinder->getHeightRP(attitude.x, attitude.y) << " "
            << last_state.xacc << " " << last_state.xvel << " " << last_state.xpos << " " << last_state.yacc << " " << last_state.yvel << " " << last_state.ypos << " " << last_state.head << " " << last_state.zvel << " " << last_state.zpos << " "
            << acc.x << " " << acc.y << " " << acc.z << " "
            << rollcmd << " " << pitchcmd << " " << yawratecmd << " " << zvelcmd << " " << rot_rollcmd << " " << rot_pitchcmd << " "
            << pose_target.x << " " << pose_target.y << " " << pose_target.z << " " << pose_target.th << " "
            << xcmd << " " << ycmd << " "
            << std::endl;
         }
         //std::cout << "  " << freq << "  ";
        
//         std::cout << "Freq = " << freq << std::endl;
            
        beats++;
                 
//         clock_gettime(CLOCK_REALTIME, &gettime_now);
//         last_heartbeat = gettime_now.tv_nsec;
//         last_time = (double)( gettime_now.tv_sec - start_log_time) + (double)gettime_now.tv_nsec/(1000000000.0); 
        
    }
}
