#ifndef CONTROLLER_BASE_HPP
#define CONTROLLER_BASE_HPP

// System Includes
#include <cmath>
#include <pthread.h>

// DJI OSDK includes
#include "dji_status.hpp"
#include <dji_vehicle.hpp>

// Helpers
#include <dji_linux_helpers.hpp>

#include "loop-heartbeat.hpp"

#include "CameraLocalizer.hpp"

#include "Rangefinder.hpp"

#include "type_defines.hpp"

#define LANDED 0
#define FLYING 1

enum GlobalEstimator {
    GPS,
    CAMERA,
    LASERSCANNER
};

class ControllerBase : public InternalThread{

protected:
    
    HeartbeatTimer loopTimer;
    Vehicle * vehicle;
    CameraLocalizer * cameraLocalizer;
    Rangefinder * rangefinder;
    
    virtual void controller_run() = 0;
    virtual void controllerMainLoop() = 0;
    // Private function
    
    void generate_Reference();
    
    void InternalThreadEntry();
    
    int loopFreq = 30;
    float Ts = 1.0/30.0;
    
    bool controller_running = false;
    bool control_action = false;
    bool integral_enable;
    
    state_t state;
    state_t last_state;

    target_pos pose_target;
    target_pos global_vel_ref;
    target_pos global_pose_ref;

    int status = 0;

    bool simulation = 0;
    
    int globalEstimatorUsed = GPS;
    
    Telemetry::GlobalPosition originGPS_Position;
    float origin_yaw = 0;
    
public:
    
    virtual bool init_controller(Vehicle * v) = 0;
    
    // Standard useful function
    
    void set_global_estimator(int globalEst);
    void set_reference(float x, float y, float z, float th);
    void set_desired_height(float z);
    void set_simulation(bool sim);
    void force_pose_target();
    void reset_position_camera();
    void reset_position_GPS();
    
    float distance_to_goal();
    bool waitForPositionReached(int timeout_s);
    
    pose_t get_position();
    target_pos get_reference();
    pose_t get_GPS_position();
    
    
    bool takeoff(float takeoff_height);
    bool land_copter();
    
    
    void enable_control_action(bool integral);
    void disable_control_action();
    bool stop_controller();
    
    
    void set_camera_localizer( CameraLocalizer * cam );
    void set_rangefinder( Rangefinder * rf );
    
};

#endif
