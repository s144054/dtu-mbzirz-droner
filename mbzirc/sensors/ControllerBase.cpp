#include "ControllerBase.hpp"
#include "loop-heartbeat.hpp"
#include "time.h"
#include "GeneralCommands.hpp"
#include <vector>
#include <cmath>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;

void ControllerBase::InternalThreadEntry()
{
    controller_running = true;
    std::cout << "InternalThreadEntry\n";
    controllerMainLoop();
    
}

bool ControllerBase::stop_controller(){
    controller_running = false;
    WaitForInternalThreadToExit();
    loopTimer.stop_hb();
}


void ControllerBase::set_camera_localizer(CameraLocalizer* cam)
{
    cameraLocalizer = cam;
}

void ControllerBase::set_rangefinder(Rangefinder* rf)
{
    rangefinder = rf;
    
}

void ControllerBase::set_simulation(bool sim)
{
    simulation = sim;
}



void ControllerBase::enable_control_action(bool integral){
    control_action = 1;
    integral_enable = integral;
}

void ControllerBase::disable_control_action(){
    control_action = 0;
    vehicle->control->xyPosZvelAndYawRateCtrl(0, 0, 0, 0);
}


void ControllerBase::generate_Reference( ) {

    float x_thresh = (global_vel_ref.x * Ts) * 1.1 ;
    float y_thresh = (global_vel_ref.y * Ts) * 1.1 ;
    float z_thresh = (global_vel_ref.z * Ts) * 1.1 ;
    float yaw_thresh = (global_vel_ref.th * Ts) * 1.1 ;
    
        // Reference generator
    if(fabs(pose_target.x - global_pose_ref.x) <= x_thresh){
        pose_target.x = global_pose_ref.x;
    }
    else{
        if(pose_target.x < global_pose_ref.x){
            pose_target.x = pose_target.x + global_vel_ref.x*Ts;
        }
        else{
            pose_target.x = pose_target.x - global_vel_ref.x*Ts;
        }
    }
    if(fabs(pose_target.y - global_pose_ref.y) <= y_thresh ){
        pose_target.y = global_pose_ref.y;
    }
    else{
        if(pose_target.y < global_pose_ref.y){
            pose_target.y = pose_target.y + global_vel_ref.y*Ts;
        }
        else{
            pose_target.y = pose_target.y - global_vel_ref.y*Ts;
        }
    }
    if(fabs(pose_target.z - global_pose_ref.z) <= z_thresh){
        pose_target.z = global_pose_ref.z;
    }
    else{
        if(pose_target.z < global_pose_ref.z){
            pose_target.z = pose_target.z + global_vel_ref.z*Ts;
        }
        else{
            pose_target.z = pose_target.z - global_vel_ref.z*Ts;
        }
    }
    if(fabs(pose_target.th - global_pose_ref.th) <= yaw_thresh){
        pose_target.th = global_pose_ref.th;
    }
    else{
        if(pose_target.th < global_pose_ref.th){
            pose_target.th = pose_target.th + global_vel_ref.th*Ts;
        }
        else{
            pose_target.th = pose_target.th - global_vel_ref.th*Ts;
        }
    }
    
}

void ControllerBase::force_pose_target(){
    
    pose_target.x = global_pose_ref.x;
    pose_target.y = global_pose_ref.y;
    pose_target.z = global_pose_ref.z;
    pose_target.th = global_pose_ref.th;
    
}

bool ControllerBase::takeoff(float target_height){
    
    float takeoff_height = last_state.zpos;
    
//     if( takeoff_height == 0 ) return 0;
    
    std::cout << "Arming motors\n";
    if( vehicle->control->armMotors(1).data ){
        std::cout << "Motors not armed\n";
    }
    sleep(5);
    
    std::cout << "Motors armed\n";
    
    for(int i = 0; i < 15; i ++){
        vehicle->control->xyPosZvelAndYawRateCtrl(0,0,1,0);
        usleep(100000);
    }
    
    float delta = 0;
    int count = 0;
    
    set_reference(last_state.xpos, last_state.ypos, target_height, last_state.head);
    force_pose_target();
    enable_control_action(false);
    
    while(1){
        if( last_state.zpos > takeoff_height + 0.1 ){
            status = FLYING;
            enable_control_action(true);
            break;
        }
        usleep(1000);
    }
    std::cout << "Copter in the air\n";

    do{
        delta = fabs( last_state.zpos - target_height );
        usleep(1000);

        if( delta < 0.1 ) count++;
    }
    while( count < 20 );
    
    std::cout << "Target height reached\n";
    
    return 1;
        
}

pose_t ControllerBase::get_GPS_position(){
    
    Telemetry::Vector3f localOffset;
    
    Telemetry::GlobalPosition currentBroadcastGP = vehicle->broadcast->getGlobalPosition();
    
    localOffsetFromGpsOffset(vehicle, localOffset,
                             static_cast<void*>(&currentBroadcastGP),
                             static_cast<void*>(&originGPS_Position));
    
    Telemetry::Quaternion broadcastQ         = vehicle->broadcast->getQuaternion();
    
    float yawInRad           = toEulerAngle((static_cast<void*>(&broadcastQ))).z;
    
    pose_t pos;
    pos.x = localOffset.x;
    pos.y = -localOffset.y;
    pos.z = localOffset.z + 0.2;
    pos.th = -(yawInRad - origin_yaw);
    if( pos.th > M_PI ) pos.th -= 2*M_PI;
    else if (pos.th < -M_PI ) pos.th += 2*M_PI;
    pos.valid = 1;
    
    return pos;
}

bool ControllerBase::land_copter()
{
    set_reference( last_state.xpos, last_state.ypos, last_state.zpos, last_state.head );
    sleep(1);
    //set_reference( last_state.xpos, last_state.ypos, 0.0, last_state.head );
    set_desired_height(0.0);
    
    std::cout << "Desending\n";
    while(1){
        usleep(1000);
        if( last_state.zpos < 0.5 ){
            std::cout << "Disabling integral controller" << std::endl;
            integral_enable = 0;
            break;
        }
    }
    
    int count = 0;
    std::cout << "Landing" << std::endl;
    while( count < 50 ){
        if( last_state.zpos < 0.35 ) count++;
        usleep(1000);
    }
    
    status = LANDED;
    
    disable_control_action();
    
    for(int i = 0; i < 15; i ++){
        vehicle->control->xyPosZvelAndYawRateCtrl(0,0,-0.5,0);
        usleep(100000);
    }
    std::cout << "Disarming motors\n";
    for(int i = 0; i < 10; i ++){
        vehicle->control->disArmMotors(1);
        usleep(10000);
    }
    
}


void ControllerBase::set_reference(float x, float y, float z, float th){
    global_pose_ref.x = x;
    global_pose_ref.y = y;
    global_pose_ref.z = z;
    global_pose_ref.th = th;
}

void ControllerBase::set_desired_height(float z){
    global_pose_ref.z = z;
}
  

float ControllerBase::distance_to_goal()
{
    float x_err = global_pose_ref.x - last_state.xpos;
    float y_err = global_pose_ref.y - last_state.ypos;
    float z_err = global_pose_ref.z - last_state.zpos;
    
    return sqrt(x_err*x_err + y_err*y_err + z_err*z_err);
}
  
bool ControllerBase::waitForPositionReached(int timeout_s)
{
    float dist_err = distance_to_goal();
    float th_err = global_pose_ref.th - last_state.head;
    
    float elapsed_time = 0;
    
    int count = 0;
    
    while( elapsed_time < timeout_s ){
        
        dist_err = distance_to_goal();
        th_err = global_pose_ref.th - last_state.head;
        
        if( dist_err < 0.2 && th_err < 5*M_PI/180 ) count++;
        
        if( count > 3 ) return 0;
        
        elapsed_time += 0.5;
        
        usleep(500000);
        
    }
    
    return false;
}
  
void ControllerBase::reset_position_camera(){
    pose_t camera_pose = cameraLocalizer->get_camera_position();
    
    last_state.xpos = camera_pose.x;
    last_state.xvel = 0;
    last_state.xacc = 0;
    last_state.ypos = camera_pose.y;
    last_state.xvel = 0;
    last_state.xacc = 0;
    last_state.head = camera_pose.th;
    //last_state.zvel = 0;
    //last_state.zpos = rangefinder->getHeight();
    
    set_reference(camera_pose.x, camera_pose.y, last_state.zpos, camera_pose.th);
    force_pose_target();
}

void ControllerBase::reset_position_GPS(){
    pose_t gps_pose = get_GPS_position();
    
    last_state.xpos = gps_pose.x;
    last_state.xvel = 0;
    last_state.xacc = 0;
    last_state.ypos = gps_pose.y;
    last_state.xvel = 0;
    last_state.xacc = 0;
    last_state.head = gps_pose.th;
    //last_state.zvel = 0;
    //last_state.zpos = rangefinder->getHeight();
    
    set_reference(gps_pose.x, gps_pose.y, last_state.zpos, gps_pose.th);
    force_pose_target();
}

pose_t ControllerBase::get_position(){
    pose_t position;
    position.x = last_state.xpos;
    position.y = last_state.ypos;
    position.z = last_state.zpos;
    position.th = last_state.head;
    return position;
}

target_pos ControllerBase::get_reference(){
    target_pos reference;
    reference.x = global_pose_ref.x;
    reference.y = global_pose_ref.y;
    reference.z = global_pose_ref.z;
    reference.th = global_pose_ref.th;
    return reference;
}


void ControllerBase::set_global_estimator(int globalEst){
    globalEstimatorUsed = globalEst;
}

