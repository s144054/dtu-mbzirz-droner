#ifndef AUTNAV_CONTROL_HPP
#define AUTNAV_CONTROL_HPP

// System Includes
#include <cmath>
#include <pthread.h>

// DJI OSDK includes
#include "dji_status.hpp"
#include <dji_vehicle.hpp>

// Helpers
#include <dji_linux_helpers.hpp>

#include "loop-heartbeat.hpp"

#include "CameraLocalizer.hpp"

#include "Rangefinder.hpp"

#include "pid.h"

#include "type_defines.hpp"

#define LANDED 0
#define FLYING 1

enum GlobalEstimator {
    GPS,
    CAMERA,
    LASERSCANNER
};

// struct target_pos{
//     float x;
//     float y;
//     float z;
//     float th;
// };
// 
// struct state_t{
//     float xacc;
//     float xvel;
//     float xpos;
//     float yacc;
//     float yvel;
//     float ypos;
//     float head;
//     float zvel;
//     float zpos;                
// 
//     float &operator[](size_t idx){
//         switch(idx){
//             case 0: return xacc;
//             case 1: return xvel;
//             case 2: return xpos;
//             case 3: return yacc;
//             case 4: return yvel;
//             case 5: return ypos;
//             case 6: return zvel;
//             case 7: return zpos;                                
//             case 8: return head;
//             default: return xacc; // Need to throw a pointer to a double
//         };
//     };
// };

class ControlAutnav : public InternalThread{

private:
    
    HeartbeatTimer loopTimer;
    
    Vehicle * vehicle;

    void autnav_run();    
    void autnav_run(float x, float y, float z, float th);    

//     void position_control_run(float x, float y, float z, float th);    
    void position_control_run(bool simulation);
    
    void force_pose_target();
    void generate_Reference();
    void autnavMainLoop();
    void InternalThreadEntry();
    
    int loopFreq = 30;
    //float Ts = 0.0025f;
    float Ts = 1.0/30.0;
    
    bool autnav_running = false;
    
    int num_states = 9;
    int num_meas   = 6;
    int num_inputs = 3;
    
    state_t state;
    state_t last_state;

    target_pos pose_target;
    target_pos global_vel_ref;
    target_pos global_pose_ref;

    int status = 0;

    float F[9][9] = {{0.0}};
    float G[9][4] = {{0.0}};
    float L[9][6] = {{0.0}};
    
    float K[3] = {{0.0}};
    float Ki = 0;
    
    float inno[6] = {0.0};

    float pitchcmd = 0;
    float rollcmd = 0;
    float yawratecmd= 0;
    
    float xcmd = 0;
    float ycmd  = 0;
    
    float zvelcmd = 0;
    
    float rot_pitchcmd = 0;
    float rot_rollcmd = 0;
    
    float ix;
    float iy;
    bool controller_enable = 0;
    bool integral_enable = 0;
    
    PID height_pid = PID(Ts, 3, -3, 0.9, 0.0, 0.0);
    PID yaw_pid = PID(Ts, 40, -40, 0.9, 0.0, 0.0);

    
    PID x_pid = PID(Ts, 1.7, -1.7, 1.2, 0.5, 0.1);
    PID y_pid = PID(Ts, 1.7, -1.7, 1.2, 0.5, 0.1);
    //PID y_pid = PID(Ts, 1, -1, 0.9, 0.01, 0.01);
    
    float pitchcmd_after_limit = 0;
    float rollcmd_after_limit = 0;
    float yawratecmd_after_limit = 0;

    long old_num_pose = 0;

    uint32_t time_last_pose_update = 0;
    
    float dummy_fw_x = 0;
    float dummy_bw_x = 0;
    
    float dummy_fw_y = 0;
    float dummy_bw_y = 0;
    
    int globalEstimatorUsed = GPS;
    
    Telemetry::GlobalPosition originGPS_Position;
    
    CameraLocalizer * cameraLocalizer;
    
    Rangefinder * rangefinder;
    
public:
    
    void set_global_estimator(int globalEst);
    
    pose_t get_position();
    
    target_pos get_reference();
    
    void PID_control_step(float x, float y, float z, float th);
    
    void set_desired_height(float z);
    
    bool takeoff(float takeoff_height);
    
    pose_t get_GPS_position();
    
    bool land_copter();
    
    void enable_controller(bool integral);

    void disable_controller();
    
    void set_camera_localizer( CameraLocalizer * cam );
    
    void set_rangefinder( Rangefinder * rf );
    
    void reset_position_camera();
    
    bool autnav_init(Vehicle * v);
    
    bool stop_autnav();
    
    void set_reference(float x, float y, float z, float th);
    
};

#endif
