#include "LaserScanner.hpp"
#include "dji_status.hpp"
#include <dji_vehicle.hpp>
#include <iostream>

bool LaserScanner::init_laser(){
 
    // *************************************
    //                 Setting up Lidar 
    
   lmssrv.port=24919;
   strcpy(lmssrv.host,"127.0.0.1");
   lmssrv.config=1;
   strcpy(lmssrv.name,"laserserver");
   lmssrv.status=1;

    // **************************************************
    //  LMS server code initialization
    //

    /* Create endpoint */

    if (lmssrv.config) {
        int errno1 = 0; 
        lmssrv.sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if ( lmssrv.sockfd < 0 )
        {
            perror(strerror(errno1));
            fprintf(stderr," Can not make  socket\n");
            exit(errno1);
            return 0;
        }

        serverconnect(&lmssrv);
    
        xmllaser=xml_in_init(4096,32);
        printf(" laserserver xml initialized \n");
    }  


    return 1;
    
}

void LaserScanner::serverconnect(componentservertype *s){
  char buf[256];
  int len;
  s->serv_adr.sin_family = AF_INET;
  s->serv_adr.sin_port= htons(s->port);
  s->serv_adr.sin_addr.s_addr = inet_addr(s->host);
  printf("port %d host %s \n",s->port,s->host);
  if ((s->connected=(connect(s->sockfd, (struct sockaddr *) &s->serv_adr, sizeof(s->serv_adr))) >-1)){
    printf(" connected to %s  \n",s->name);
    len=sprintf(buf,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    send(s->sockfd,buf,len,0);
    len=sprintf(buf,"mrc version=\"1.00\" >\n");
    send(s->sockfd,buf,len,0);
    if (fcntl(s->sockfd,F_SETFL,O_NONBLOCK) == -1) {
          fprintf(stderr,"startserver: Unable to set flag O_NONBLOCK on %s fd \n",s->name);
    }
  }
  else{
      printf("Not connected to %s  %d \n",s->name,s->connected);   
  }
}

void LaserScanner::getData(){

	laserpar[0]=0;
	while(laserpar[0]==0)
	{
		while ( (xml_in_fd(xmllaser,lmssrv.sockfd) >0))
		//printf("xmlprica");
		xml_proca(xmllaser);
		usleep(100000);
	}
}


void LaserScanner::xml_proca(struct xml_in *x){

    while(1){
    switch (xml_in_nibble(x)) {
        case XML_IN_NONE:
            return;
        case XML_IN_TAG_START:
            //printf("Start tage\n");
            #if (0)
            {int i;
            double a;
                printf("start tag: %s, %d attributes\n", x->a, x->n);
                for(i=0;i<x->n;i++){
                printf("  %s    %s  \n",x->attr[i].name,x->attr[i].value);
                }
            }
            #endif
                if (strcmp("laser",x->a)==0){
                int i,ix;
                for (i=0;i< x->n;i++){
                    ix=atoi(x->attr[i].name+1);
                    if (ix >-1 && ix < 10)
                        laserpar[ix]=atof(x->attr[i].value);
                }   
            }
            
            break;
        case XML_IN_TAG_END:
            //printf("end tag: %s\n", x->a);
            break;
        case XML_IN_TEXT:
            //printf("text: %d bytes\n  \"", x->n);
            //fwrite(x->a, 1, x->n, stdout);
            //printf("\"\n");
            break;
        }   
    } 
}  


void LaserScanner::test_zoneobst(){
    len=sprintf(buf,"<scanpush cmd='zoneobst'/>\n"); //  total=100
    send(lmssrv.sockfd,buf,len,0);
    sleep(1);
    getData();
    
    std::cout << "zoneobst data = ";
    for(int i = 0; i < 10; i++ ){
        std::cout << laserpar[i] << "  ";
    }
    std::cout << std::endl;
}
