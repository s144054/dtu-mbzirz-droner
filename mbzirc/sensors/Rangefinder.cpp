#include "Rangefinder.hpp"
#include <iostream>
#include <bitset>
#include <cmath>

bool Rangefinder::is_enabled(){
    return enabled;
}

/*! Initializing the I2C module
!*/

bool Rangefinder::initializeI2C(){
    
		int addr = 0x30;          //<<<<<The I2C address of the Terraranger
		
		//----- OPEN THE I2C BUS -----
		char *filename = (char*)"/dev/i2c-1";
		if ((file_i2c = open(filename, O_RDWR)) < 0)
		{
			std::cout << "Failed to open the i2c bus" << std::endl;
			enabled = 0;
		} 
		else if (ioctl(file_i2c, I2C_SLAVE, addr) < 0)
		{
			std::cout << "Failed to acquire bus access and/or talk to slave.\n" << std::endl;
			enabled = 0;
		}
		else {
			std::cout << "I2C device linked" << std::endl;
			enabled = 1;
		}
		
		return enabled;
}

void Rangefinder::set_in_use(bool val){
    in_use = val;
}

/*! Checks if there is still connection with the Terraranger. If not then the function returns height data measured by the Matrice
!*/

 float Rangefinder::getHeight( ){
        if( !in_use) return 0;
        
		int length=3; //<<< Number of bytes to read
		unsigned char buffer[10] = {0};
		unsigned long distance;
		float distanceFloat = 0;
		if( enabled ){
			//read() returns the number of bytes actually read, if it doesn't
			//match then an error occurred (e.g. no response from the device)
            int resp = read(file_i2c, buffer, length);
            if( resp != length )
// 			if (read(file_i2c, buffer, length) != length)		
			{
				std::cout<<"Lost connection to Terraranger. Using Barometer instead : " << resp << std::endl;
// 				enabled = 0;
			}
			else
			{
				std::bitset<8> x(buffer[0]);
				distance=x.to_ulong();
				distance=distance<<8;
				x=buffer[1];
				distance=distance|x.to_ulong();
				distanceFloat = ((float)distance)/1000;
			}
		} else {
			std::cout << "Rangefinder not connected, run initializeI2C()\n";
		}
		return distanceFloat;
 }

  float Rangefinder::getHeightRP( float roll, float pitch ){
        if( !in_use) return 0;
		
        int length=3; //<<< Number of bytes to read
		unsigned char buffer[10] = {0};
		unsigned long distance;
		float distanceFloat = 0;
		if( enabled ){
			//read() returns the number of bytes actually read, if it doesn't
			//match then an error occurred (e.g. no response from the device)
            int resp = read(file_i2c, buffer, length);
            if( resp != length )
// 			if (read(file_i2c, buffer, length) != length)		
			{
                std::cout<<"Lost connection to Terraranger. Using Barometer instead : " << resp << std::endl;
// 				enabled = 0;
			}
			else
			{
				std::bitset<8> x(buffer[0]);
				distance=x.to_ulong();
				distance=distance<<8;
				x=buffer[1];
				distance=distance|x.to_ulong();
				distanceFloat = ((float)distance)/1000;
			}
		} else {
			std::cout << "Rangefinder not connected, run initializeI2C()\n";
		}
		
		distanceFloat *= cos(roll)*cos(pitch);
		
		return distanceFloat;
 }
