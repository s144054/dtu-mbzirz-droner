#include "PIDPositionControl.hpp"
#include "GeneralCommands.hpp"


using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;

bool PIDPositionControl::init_controller(Vehicle * v)
{
    vehicle = v;
    
    uint8_t broadcast_freq[16];
    vehicle->broadcast->setVersionDefaults(broadcast_freq);
    broadcast_freq[1] = DataBroadcast::FREQ_50HZ; // attitude
    broadcast_freq[2] = DataBroadcast::FREQ_50HZ; // acceleration
    broadcast_freq[3] = DataBroadcast::FREQ_0HZ; // linear velocity
    broadcast_freq[4] = DataBroadcast::FREQ_50HZ; // gyro
    broadcast_freq[5] = DataBroadcast::FREQ_50HZ; // gps location
    broadcast_freq[6] = DataBroadcast::FREQ_0HZ; // magnetometer
    //broadcast_freq[7] = DataBroadcast::FREQ_5HZ; // remote controller data
    broadcast_freq[8] = DataBroadcast::FREQ_0HZ; // Gimbal
    //broadcast_freq[9] = DataBroadcast::FREQ_50HZ; // flightstatus
    //broadcast_freq[10] = DataBroadcast::FREQ_50HZ; //battery
    //broadcast_freq[11] = DataBroadcast::FREQ_50HZ; //control device
    vehicle->broadcast->setBroadcastFreq(broadcast_freq);
    
    // Reset state vector
    for(int i = 0; i<num_states; i++){
        state[i] = 0.0f;
        last_state[i] = 0.0f;
    }

    // Reset innovation
    for(int i = 0; i<num_meas; i++){
        inno[i] = 0.0f;
    };

    // Reset target pose
    pose_target.x = 0.0f;
    pose_target.y = 0.0f;
    pose_target.z = 0.0f;
    pose_target.th = 0.0f;
    
    integral_enable = 0;
    
    // System matrix
    F[0][0]=0.8878; F[0][1]=0.0f;    F[0][2]=0.0f; 
    F[1][0]=0.0314; F[1][1]=1.0f;    F[1][2]=0.0f; 
    F[2][0]=0.0005; F[2][1]=0.0333;  F[2][2]=1.0f; 

    F[3][3]=0.8878; F[3][4]=0.0f;    F[3][5]=0.0f; 
    F[4][3]=0.0314; F[4][4]=1.0f;    F[4][5]=0.0f; 
    F[5][3]=0.0005; F[5][4]=0.0333;  F[5][5]=1.0f; 

    F[6][6]=1.0;    F[6][7]=0.0f;    F[6][8]=0.0f; 
    F[7][6]=0.0333; F[7][7]=1.0f;    F[7][8]=0.0f; 
    F[8][6]=0.0;    F[8][7]=0.0;     F[8][8]=1.0f; 

    // Input matrix
    G[0][0]= 1.1021;
    G[1][0]= 0.0187; 
    G[2][0]= 0.0002; 

    G[3][1]= 1.1021;
    G[4][1]= 0.0187; 
    G[5][1]= 0.0002; 
    
    G[6][2] = 0.0333;
    G[7][2] = 0.0006;
    G[8][3] = 0.0333;

    for( int i = 0; i < num_states; i++ ){
        for( int j = 0; j < num_meas; j++ ){
            L[i][j] = 0.0f;
        }
    }
    
    L[0][0] = 0.9403;
    L[0][1] = 0.0238;
    L[1][0] = 0.0016;
    L[1][1] = 0.8728;
    L[2][1] = 0.2267;
    
    L[3][2] = 0.9403;
    L[3][3] = 0.0238;
    L[4][2] = 0.0016;
    L[4][3] = 0.8728;
    L[5][3] = 0.2267;
    
    L[6][4] = 0.2941;
    L[7][4] = 0.1352;
    
    L[8][5] = 0.6180;

    global_vel_ref.x = 2;
    global_vel_ref.y = 2;
    global_vel_ref.z = 1;
    global_vel_ref.th = 40*M_PI/180;
    
    originGPS_Position = vehicle->broadcast->getGlobalPosition();

    Telemetry::Quaternion quaterion = vehicle->broadcast->getQuaternion();
    Telemetry::Vector3f attitude = toEulerAngle(&quaterion);    
    origin_yaw = attitude.z * 0;
    
    loopTimer.start_hb(loopFreq);
    
    if( !StartInternalThread() ){
        std::cout << "Autnav control thread not started\n";
        return false;
    }
    
    return true;
}
    
void PIDPositionControl::controller_run()
{
        
    pose_t globalPos;
    
    if( globalEstimatorUsed == CAMERA ) globalPos = cameraLocalizer->get_camera_position();
    else if( globalEstimatorUsed == GPS ) globalPos = get_GPS_position();
    
    Telemetry::Quaternion quaterion = vehicle->broadcast->getQuaternion();
    Telemetry::Vector3f attitude = toEulerAngle(&quaterion);
    Telemetry::Vector3f acc = vehicle->broadcast->getAcceleration();
    Telemetry::Vector3f gyro = vehicle->broadcast->getAngularRate();

    for(int i = 0; i<num_states; i++){
        state[i] = 0.0f;
    }
    
    // Kalman prediction
    for(int i = 0; i<num_states; i++){
        state.xacc += F[0][i]*last_state[i];
        state.xvel += F[1][i]*last_state[i];
        state.xpos += F[2][i]*last_state[i];
        state.yacc += F[3][i]*last_state[i];
        state.yvel += F[4][i]*last_state[i];
        state.ypos += F[5][i]*last_state[i];
        state.zvel += F[6][i]*last_state[i];
        state.zpos += F[7][i]*last_state[i];
        state.head += F[8][i]*last_state[i];
    }

    // Inject input to model

    state.xacc += G[0][0]*( attitude.x*M_PI/180.0 );
    state.xvel += G[1][0]*( attitude.x*M_PI/180.0 );
    state.xpos += G[2][0]*( attitude.x*M_PI/180.0 );
    state.yacc += G[3][1]*( attitude.y*M_PI/180.0 );
    state.yvel += G[4][1]*( attitude.y*M_PI/180.0 );
    state.ypos += G[5][1]*( attitude.y*M_PI/180.0 );
    
    state.zvel += G[6][2]*(acc.z); 
    state.zpos += G[7][2]*(acc.z);           
    state.head += G[8][3]*(-gyro.z);   
    
    acc = rotation_pixhawk_to_world(acc.x, acc.y, acc.z, last_state.head);
    // Innovation using acceleration measurement
    inno[0] = acc.x - last_state.xacc;
    inno[2] = acc.y - last_state.yacc;
    
    if( simulation && globalEstimatorUsed != GPS ) globalPos.z = get_GPS_position().z; 
    else if( !simulation ) globalPos.z = rangefinder->getHeightRP(attitude.x, attitude.y);
    
    if( globalPos.z != 0 ){
        inno[4] = (globalPos.z - last_state.zpos);
    }
    else{
        inno[4] = 0;
    }

    if( globalPos.valid ) {
        inno[1] = globalPos.x - last_state.xpos;
        inno[3] = globalPos.y - last_state.ypos; 
        inno[5] = globalPos.th - last_state.head;
    }
    else {
        inno[1] = 0;
        inno[3] = 0; 
        inno[5] = 0;
    }
    
    // Kalman update
    for(int i = 0; i<num_meas; i++){
        state.xacc += L[0][i]*inno[i];
        state.xvel += L[1][i]*inno[i];
        state.xpos += L[2][i]*inno[i];
        state.yacc += L[3][i]*inno[i];
        state.yvel += L[4][i]*inno[i];
        state.ypos += L[5][i]*inno[i];
        state.zvel += L[6][i]*inno[i];
        state.zpos += L[7][i]*inno[i];
        state.head += L[8][i]*inno[i];
    }
    
    // Update last state
    last_state.xacc = state.xacc;
    last_state.xvel = state.xvel;
    last_state.xpos = state.xpos;
    last_state.yacc = state.yacc;
    last_state.yvel = state.yvel;
    last_state.ypos = state.ypos;
    last_state.zvel = state.zvel;
    last_state.zpos = state.zpos;
    last_state.head = state.head;
    
    if( control_action ) {
    
        generate_Reference();
        
        yawratecmd = -yaw_pid.calculate(pose_target.th, state.head) * 180/M_PI;

        zvelcmd = height_pid.calculate(pose_target.z, state.zpos);

        xcmd = x_pid.calculate(pose_target.x, state.xpos);

        ycmd = y_pid.calculate(pose_target.y, state.ypos);

        float rot_yaw = state.head - origin_yaw;
        if( rot_yaw > M_PI ) rot_yaw -= 2*M_PI;
        else if ( rot_yaw < -M_PI ) rot_yaw += 2*M_PI;
        
        Vector3f control_signal = rotation_world_to_pixhawk(xcmd, ycmd, 0, rot_yaw );
        xcmd = control_signal.x;
        ycmd = -control_signal.y;
        vehicle->control->xyPosZvelAndYawRateCtrl(xcmd, ycmd, zvelcmd, yawratecmd);

    }

}

void PIDPositionControl::switch_to_camera_localizer(){
    
    reset_position_camera();
    set_global_estimator(CAMERA);
    yaw_pid.reset_control();
    x_pid.reset_control();
    y_pid.reset_control();
    
}

void PIDPositionControl::switch_to_GPS_localizer(){
    
    reset_position_GPS();
    set_global_estimator(GPS);
    yaw_pid.reset_control();
    x_pid.reset_control();
    y_pid.reset_control();
    
}

void PIDPositionControl::controllerMainLoop()
{
    long beats = 0;
    struct timespec gettime_now; 
    
    double last_time;
    double new_time;
    double freq;
    
    Vector3f velocity;
    Vector3f attitude;
    Quaternion atti_quad;
    float height;
    Telemetry::Vector3f acc;
    pose_t camera_pose;
    
    clock_gettime(CLOCK_REALTIME, &gettime_now);
    long start_log_time = gettime_now.tv_sec;
    
    last_time = (double)( gettime_now.tv_sec - start_log_time) + (double)gettime_now.tv_nsec/(1000000000); 
    
    char str[80];
	char buf[16]; // need a buffer for that
	int i=1;
	while (true){
        sprintf(buf,"%d",i);
		const char* p = buf;
		strcpy (str,"/home/local/DJI_Matrice100_Ananda/Log/output");
		strcat (str,p);
		strcat (str,".txt");
		std::ifstream fileExist(str);
		if(!fileExist)
        {
			break;
		}
		i++;
	}
    std::freopen( str, "w", stderr );
    std::cerr.precision(10);
    
    while( controller_running ) {
        
        while( !loopTimer.get_hb_flag() ) usleep(20);
        
        controller_run();
        
        if( beats % 30 == 0 ){
            if(globalEstimatorUsed == GPS){
                pose_t gps_pos = get_GPS_position();
                std::cout << "GPS:   X = " << gps_pos.x << "\tY = " << gps_pos.y << "\tZ = " << gps_pos.z << "\tTh = " << gps_pos.th*180/M_PI << std::endl;
            }
            else if( globalEstimatorUsed == CAMERA ){
                camera_pose = cameraLocalizer->get_camera_position();
                std::cout << "Camer: X = " << camera_pose.x << "\tY = " << camera_pose.y << "\tZ = " << height << "\tTh = " << camera_pose.th*180/M_PI << std::endl;
            }
            
            std::cout << "State: X = " << last_state.xpos << "\tY = " << last_state.ypos << "\tZ = " << last_state.zpos << "\tTh = " << last_state.head *180/M_PI << std::endl;
        }

        clock_gettime(CLOCK_REALTIME, &gettime_now);
        new_time = (double)( gettime_now.tv_sec - start_log_time) + (double)gettime_now.tv_nsec/(1000000000.0); 
        freq = 1.0/(new_time - last_time);
        
        last_time = new_time;
        
        if( beats % 1 == 0 ) {
            velocity = vehicle->broadcast->getVelocity();
            atti_quad = vehicle->broadcast->getQuaternion();
            attitude = toEulerAngle( &atti_quad );
            height = vehicle->broadcast->getGlobalPosition().altitude;
            camera_pose = cameraLocalizer->get_camera_position();
            
            acc = vehicle->broadcast->getAcceleration();
            
            std::cerr << new_time << " " << freq
            << " " << attitude.x << " " << attitude.y << " " << attitude.z << " "
            << global_pose_ref.x << " " << global_pose_ref.y << " " << global_pose_ref.z << " " << global_pose_ref.th << " "
            << camera_pose.x << " " << camera_pose.y << " " << camera_pose.z << " " << camera_pose.th << " " << camera_pose.valid << " "
            << height << " " << rangefinder->getHeight() << " " << rangefinder->getHeightRP(attitude.x, attitude.y) << " "
            << last_state.xacc << " " << last_state.xvel << " " << last_state.xpos << " " << last_state.yacc << " " << last_state.yvel << " " << last_state.ypos << " " << last_state.head << " " << last_state.zvel << " " << last_state.zpos << " "
            << acc.x << " " << acc.y << " " << acc.z << " "
            << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " "
            << pose_target.x << " " << pose_target.y << " " << pose_target.z << " " << pose_target.th << " "
            << xcmd << " " << ycmd << " "
            << std::endl;
        }
        
        if(camera_pose.valid) std::cout << "##########\n##\n#######\n";
        
        beats++;
        
    }    
}
    
