/*! @file flight-control/main.cpp
 *  @version 3.3
 *  @date Jun 05 2017
 *
 *  @brief
 *  main for Flight Control API usage in a Linux environment.
 *  Provides a number of helpful additions to core API calls,
 *  especially for position control, attitude control, takeoff,
 *  landing.
 *
 *  @Copyright (c) 2016-2017 DJI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */ 
 //K-develope

// DJI OSDK includes
#include "dji_status.hpp"
#include <dji_vehicle.hpp>
// Helpers
#include <dji_linux_helpers.hpp>

#include "CameraLocalizer.hpp"
#include "Rangefinder.hpp"
#include "LaserScanner.hpp"
#include "autnav_control.hpp"

#include "flight_control_sample.hpp"

#include <cmath>

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;


/*! main
 *
 */
int
main(int argc, char** argv)
{

  // Initialize variables
  int functionTimeout = 1;

  // Setup OSDK.
  LinuxSetup linuxEnvironment(argc, argv);
  Vehicle*   vehicle = linuxEnvironment.getVehicle();
  if (vehicle == NULL)
  {
    std::cout << "Vehicle not initialized, exiting.\n";
    return -1;
  }

  // Obtain Control Authority
  vehicle->obtainCtrlAuthority(functionTimeout);
  
  // Start sensors  
  

  // Start Camera
  CameraLocalizer downCamera(320,240);//(640,480);
  if( !downCamera.init_camera(vehicle) ) {
      std::cout << "Camera couldn't start\n";
      return 0;
  }
  // Sleep to ensure camera functioning properly
  sleep(1);
  downCamera.set_loopFreq(30);
  

  Rangefinder rangefinder;
  if( !rangefinder.initializeI2C() ) {
    std::cout << "Rangefinder not connected\n";
  }
  
  // Initialize the controller and Kalman filter model
  ControlAutnav controller;
  controller.autnav_init(vehicle);
  
  // Add sensors to the controller and reset position to camera
  controller.set_camera_localizer( &downCamera );
  controller.set_rangefinder( &rangefinder );
  //controller.reset_position_camera();
  controller.set_reference(0, 0, 0, 0);
  
  pose_t camera_pose = downCamera.get_camera_position();
  pose_t position = controller.get_position();
  target_pos reference = controller.get_reference();
  
  std::cout << "#######################################\n" << std::endl;
  std::cout << "Rangefinder height = " << rangefinder.getHeight() << " m\n\n";
  std::cout << "Camera position:\nX = " << camera_pose.x << " m\nY = " << camera_pose.y << " m\nTh = " << camera_pose.th*180/M_PI << " deg\nValid = " << camera_pose.valid << "Num pose = " << camera_pose.num_pose << "\n\n";
  std::cout << "Kalman position:\nX = " << position.x << " m\nY = " << position.y << " m\nTh = " << position.th*180/M_PI << " deg\n\n";
  std::cout << "Reference position:\nX = " << reference.x << " m\nY = " << reference.y << " m\nTh = " << reference.th*180/M_PI << " deg\n\n";
  std::cout << "######################################\n";
  
  // Display interactive prompt
	std::cout << "| Available commands:        |" << std::endl;
	std::cout << "| Press [a]: Start Stay At with GPS start and landing   |" << std::endl;
    std::cout << "| Press [b]: ???" << std::endl;
    std::cout << "| Press [c]: Fly manually with sensors reading and model running (no autnav control) |" << std::endl;
	
    pose_t kalman_pos;
    float zvelcmd = 0;
    
    char inputChar;
	std::cin >> inputChar;

    
    VirtualRCData vrcData;
    double timeOut = 1000000;
    int elapsedTime = 0;
    
  switch(inputChar){
    case 'a':
//         downCamera.stop_camera();
        monitoredTakeoff(vehicle);
        
        if( downCamera.get_camera_position().valid ) {
            
        
            std::cout << "####################\n!!! Starting camera aided flight !!!\n###################\n";
            camera_pose = downCamera.get_camera_position();
            controller.set_desired_height(1.7);
            //controller.reset_position_camera();
            controller.enable_controller(true);
            
            for(int i = 0; i < 20*5; i++){
                usleep(200000);
                if( i % 5 == 0 ) std::cout << "Time = " << i*0.2 << " seconds\n";
//                 if(i == 2*5) {
//                     controller.set_reference(0, 0, rangefinder.getHeight(), -M_PI/4);
//                 }
//                 if(i == 7*5) {
//                     controller.set_reference(2, 1, rangefinder.getHeight(), M_PI/7);
//                 }
            }
            std::cout << "####################\n!!! Stopping camera aided flight !!!\n###################\n";
            controller.disable_controller();
        }
        monitoredLanding(vehicle);
        
            
        break;
    case 'b':
        std::cout << "Starting mission\n";

//         moveByPositionOffset( vehicle, 1, 1, 0, 0, 0.1, 0.1);
        
        vehicle->control->positionAndYawCtrl(0,0,1,0);
        for(int i = 1; i <= 10*50 ; i++){
            vehicle->control->positionAndYawCtrl(3,0,1,0);
            usleep(20000);
            std::cout << 0.02*i << " seconds\n";
        }
        break;

    case 'c':
        vehicle->releaseCtrlAuthority(functionTimeout);
        std::cout << "Manual flight chosen Fly for 30 seconds\n";
        for(int i = 1; i <= 30*5 ; i++){
            usleep(200000);
            std::cout << 0.2*i << " seconds\n";
        }
        std::cout << "Data logging over. Closing program\n";
        
        break;
    case 'd':
        
        std::cout << "Takeoff started\n";
        monitoredTakeoff(vehicle);
        
        std::cout << "Testing control values\n";
        controller.set_reference(1,0,0,0);
        for(int i = 0; i < 100*5; i++){
            
            kalman_pos = controller.get_position();
            
            zvelcmd = 0.9 * ( 1.3 - kalman_pos.z );

            vehicle->control->attitudeYawRateAndVertVelCtrl(0, 4, 0, zvelcmd);
            
            usleep( 10000 );
        
        }
        
        std::cout << "Testing control values\n";
        monitoredLanding(vehicle);
        
        break;
    case 'e':
        controller.set_global_estimator(GPS);
        vehicle->obtainCtrlAuthority(functionTimeout);
        //downCamera.set_loopFreq(10);
        controller.set_reference(0, 0, 0, 0);
        controller.set_desired_height(2.2);
        std::cin >> inputChar;
        std::cout << "Controller on \n";
        controller.enable_controller(1);
//         sleep(3);
        std::cin >> inputChar;
        std::cout << "Setting reference 1\n";
        controller.set_reference(-0.052607730030000, 0, 2.2, 0);
        std::cin >> inputChar;
        std::cout << "Setting reference\n";
        controller.set_reference(2, -0.052607730030000, 2.2, 0);
        std::cin >> inputChar;
        std::cout << "Setting reference\n";
        controller.set_reference(0, 0, 2.2, 0);
        std::cin >> inputChar;
        std::cout << "Setting reference\n";
        controller.set_reference(-1, -1, 1, 0.5);
        std::cin >> inputChar;
        controller.disable_controller();
        
        usleep(200000);
        break;
    case 'f':
        
        std::cout << "Full run test\n";
        controller.set_global_estimator(CAMERA);
        std::cin >> inputChar;
        
        std::cout << "Takeoff\n";
        controller.takeoff(2.3);
        std::cin >> inputChar;
        
        std::cout << "Landing\n";
        controller.land_copter();
        
        std::cout << "Task complete\n";
        
        break;
    case 'g':
        vehicle->virtualRC->resetVRCData();
    
//     std::cout << vrcData.throttle << "  " << vrcData.roll << std::endl;
        std::cout << "Turning on virtual\n";
        vehicle->virtualRC->setControl(true, VirtualRC::CutOff_ToRealRC);
        
        std::cout << "Checking if virtual is on\n";

        while( elapsedTime < timeOut ){
            
            if( vehicle->virtualRC->isVirtualRC() ){
                std::cout << "Virtual ON" << std::endl;
                break;
            }
            usleep(1000);
            elapsedTime += 1000;
            
        }
        if( elapsedTime > timeOut ){
            std::cout << "Virtual timeout reached" << std::endl;
            return 0;   
        }
        
        std::cout << "Arming drone\n"; 
        vrcData = vehicle->virtualRC->getVRCData();
        
        vrcData.throttle = 1024;
        vrcData.roll = 1024;
        vrcData.pitch = 1024;
        vrcData.yaw = 1024;
        vrcData.mode = 1024;
        for(int i = 0; i < 200*3; i++){
            vehicle->virtualRC->sendData(vrcData);
            usleep( 5000 );
        }
        vrcData.roll = 1024 + 90;
        controller.set_reference(2, 0, 0, 0);
        for(int i = 0; i < 200*3; i++){
            vehicle->virtualRC->sendData(vrcData);
            usleep( 5000 );
        }
        vrcData.roll = 1024 + 150;
        controller.set_reference(4, 0, 0, 0);
        for(int i = 0; i < 200*3; i++){
            vehicle->virtualRC->sendData(vrcData);
            usleep( 5000 );
        }
        
        break;
    default:
      break;
  }
  
  downCamera.stop_camera();
  controller.stop_autnav();
  
  vehicle->releaseCtrlAuthority(functionTimeout);
  
  return 0;
}
